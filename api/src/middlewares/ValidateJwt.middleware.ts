import { NextFunction, Request, Response } from 'express';
import * as jwt from "jsonwebtoken";
import { JWT_SECRET } from '../config/constants';
import { getJwt } from '../utils/Jwt.util';

export async function validateJwt(request: Request, response: Response, next: NextFunction) {
  const token = getJwt(request);

  if (!token) {
    return response.status(401).json({
      success: false,
      message: 'Error obteniendo el token'
    })
  }

  try {
    const { usuario } = jwt.verify(token, JWT_SECRET) as any;

    request['usuario'] = usuario;
  } catch (error) {
    return response.status(401).json({
      success: false,
      message: 'Token no valido'
    })
  }

  next();
}
