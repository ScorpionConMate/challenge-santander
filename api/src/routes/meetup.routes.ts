import express, { request, Request, response, Response } from "express";
import { MeetupController } from "../controllers/Meetup.controller";
import { isAdmin } from '../middlewares/IsAdmin.middleware';
import { validateJwt } from '../middlewares/ValidateJwt.middleware';
import { CatchError } from '../utils/Errors.util';

const router = express.Router();

router.get("/", async (req: Request, res: Response) => {
  const controller = new MeetupController();
  const response = await controller.getAll();
  return res.json(response);
});

router.get("/:slug", async (req: Request, res: Response) => {
  const { slug } = req.params;
  const controller = new MeetupController();
  const response = await controller.getOne(slug);
  return res.json(response);
});

router.post("/new", [isAdmin, validateJwt], async (req: Request, res: Response) => {
  const controller = new MeetupController();
  const response = await controller.create(req, req.body);
  return res.json(response);
});

router.post("/subscribe/:meetup", [validateJwt], async (req: Request, res: Response) => {
  try {
    const controller = new MeetupController();
    const response = await controller.subscribe(req);
    return res.status(response.statusCode).json(response);
  } catch (error) {
    return response.status(500).json({ success: false, message: CatchError(error) });
  }
});

router.get("/realizar-pedido/:token", async (req: Request, res: Response) => {
  try {
    const { token } = req.params;
    const controller = new MeetupController()
    const response = await controller.makeOrderBirras(token);
    res.send(response)
  } catch (error) {
    return res.status(500).json({ success: false, message: "Ocurrio un error, contactese con el administrador" });
  }
});

router.post("/my-meetups", [validateJwt], async (req: Request, res: Response) => {
  try {
    const controller = new MeetupController();
    const response = await controller.myMeetups(req)
    res.send(response)
  } catch (error) {
    console.log(error)
    return res.status(500).json({ success: false, message: "Ocurrio un error, contactese con el administrador" });
  }
})

export default router;
