import { EventSubscriber, EntitySubscriberInterface, InsertEvent, UpdateEvent } from "typeorm";
import { Meetup } from '../models/Meetup';

@EventSubscriber()
export class UserSubscriber implements EntitySubscriberInterface<Meetup> {

  listenTo() {
    return Meetup;
  }

  beforeInsert(event: InsertEvent<Meetup>) {
    if (event.entity instanceof Meetup) {
      return event.entity.slugify();
    }
  }
}
