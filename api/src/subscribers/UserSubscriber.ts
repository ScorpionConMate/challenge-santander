import { EventSubscriber, EntitySubscriberInterface, InsertEvent } from "typeorm";
import { User } from '../models/User';

@EventSubscriber()
export class UserSubscriber implements EntitySubscriberInterface<User> {
  
  listenTo() {
    return User;
  }

  beforeInsert(event: InsertEvent<User>) {
    if (event.entity instanceof User) {
      return event.entity.hashPassword()
    }
  }
}
