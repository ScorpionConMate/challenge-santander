import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity("meetup_clima")
export class MeetupClima extends BaseEntity {
  @PrimaryGeneratedColumn()
  id?: string;

  @Column({type: 'datetime'})
  date: Date;

  @Column({type: 'bool'})
  rain: boolean;

  @Column({type: 'float'})
  tempAprox: number;
}
