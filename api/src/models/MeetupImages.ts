import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('meetup_images')
export class MeetupImages extends BaseEntity {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({type: "varchar"})
  imagePath: string;
}
