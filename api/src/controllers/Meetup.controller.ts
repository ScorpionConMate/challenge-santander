import { Tags, Route, Get, Post, Request, Body, Path, Response, Example, Security } from 'tsoa';
import { Request as Req } from "express";
import { Meetup } from '../models/Meetup';
import { MeetupRepository, IMeetupPayload } from '../respositories/Meetup.repository';
import { UserService } from '../services/User.service';
import { BirrasMeetupService } from '../services/BirrasMeetup.service';
import { IErrorGeneric, IMeetupCreated, IMeetupResponse, ISuccessGeneric } from '../interfaces/ReponsesExamples.interface';

@Route("api/meetup")
@Tags("Meetup")
export class MeetupController extends UserService {

  @Response<IMeetupResponse[]>(200, "Meetup List", [
    {
      "id": 151,
      "nombre": "Global Program Producer",
      "fecha": "2021-03-15T03:32:33.000Z",
      "duracion": "1h",
      "descripcion": "Fuga in dolor qui facilis minima. Enim cumque et. Placeat repellat sit sapiente consequatur ipsum. Ad quam dolores itaque libero odio maxime quaerat unde. Consequatur sed rem.",
      "slug": "global-program-producer-1615519953641",
      "lugar": "Catamarca, AR",
      "createdAt": new Date(),
      "updatedAt": new Date(),
      "available": true,
      "organizador": {
        "email": "admin4@birras.com",
        "nombre": "Immanuel",
        "apellido": "Roob"
      },
      "image": {
        "id": 154,
        "imagePath": "https://dummyimage.com/1920x420/20157f/582865"
      },
      "clima": {
        "id": 13,
        "date": new Date(),
        "rain": false,
        "tempAprox": 24.3
      }
    },
  ])
  @Response<IErrorGeneric>(500, "Meetup list get failed", {
    success: false,
    message: "Ocurrio un error, contacte con el Administrador del sistema",
  })
  @Get('/')
  public async getAll(): Promise<IMeetupResponse[] | IErrorGeneric> {
    try {
      return MeetupRepository.getMeetups();
    } catch (error) {
      return { statusCode: 500, success: false, message: 'Ocurrio un error, contacte con el Administrador del sistema' } as IErrorGeneric;
    }
  }

  @Response<IMeetupResponse>(200, "Meetup by slug", {
    "id": 151,
    "nombre": "Global Program Producer",
    "fecha": "2021-03-15T03:32:33.000Z",
    "duracion": "1h",
    "descripcion": "Fuga in dolor qui facilis minima. Enim cumque et. Placeat repellat sit sapiente consequatur ipsum. Ad quam dolores itaque libero odio maxime quaerat unde. Consequatur sed rem.",
    "slug": "global-program-producer-1615519953641",
    "lugar": "Catamarca, AR",
    "createdAt": new Date(),
    "updatedAt": new Date(),
    "available": true,
    "organizador": {
      "email": "admin4@birras.com",
      "nombre": "Immanuel",
      "apellido": "Roob"
    },
    "image": {
      "id": 154,
      "imagePath": "https://dummyimage.com/1920x420/20157f/582865"
    },
    "clima": {
      "id": 13,
      "date": new Date(),
      "rain": false,
      "tempAprox": 24.3
    }
  })
  @Response<IErrorGeneric>(500, "Meetup get by slug failed", {
    success: false,
    message: "Ocurrio un error, contacte con el Administrador del sistema",
  })
  @Get('{slug}')
  public async getOne(@Path() slug: string): Promise<IErrorGeneric | IMeetupResponse> {
    try {
      return MeetupRepository.getMeetupBySlug(slug);
    } catch (error) {
      return { statusCode: 500, success: false, message: 'Ocurrio un error, contacte con el Administrador del sistema' } as IErrorGeneric;
    }
  }

  @Post('/new')
  @Response<IMeetupCreated>(200, "Meetup created", {
    "id": 151,
    "nombre": "Global Program Producer",
    "fecha": "2021-03-15T03:32:33.000Z",
    "duracion": "1h",
    "descripcion": "Fuga in dolor qui facilis minima. Enim cumque et. Placeat repellat sit sapiente consequatur ipsum. Ad quam dolores itaque libero odio maxime quaerat unde. Consequatur sed rem.",
    "slug": "global-program-producer-1615519953641",
    "lugar": "Catamarca, AR",
    "createdAt": new Date(),
    "updatedAt": new Date(),
    "available": true,
    "organizador": {
      "email": "admin4@birras.com",
      "nombre": "Immanuel",
      "apellido": "Roob"
    }
  })
  @Response<IErrorGeneric>(500, "Meetup get by slug failed", {
    success: false,
    message: "Ocurrio un error, contacte con el Administrador del sistema",
  })
  @Security('api_key')
  public async create(@Request() request: Req, @Body() body?: IMeetupPayload): Promise<IMeetupResponse | IErrorGeneric> {
    try {
      return MeetupRepository.createMeetup(request, body);
    } catch (error) {
      return { statusCode: 500, success: false, message: 'Ocurrio un error, contacte con el Administrador del sistema' } as IErrorGeneric;
    }
  }

  @Post('/subscribe/{meetup}')
  @Response<ISuccessGeneric>(200, "Subscribe success", {
    statusCode: 200,
    success: true,
    message: "Inscripcion exitosa"
  })
  @Response<IErrorGeneric>(500, "Subscribe failed", {
    success: false,
    message: "Ocurrio un error, contacte con el Administrador del sistema",
  })
  @Security('api_key')
  public async subscribe(@Request() request: Req, @Path() meetup?: number): Promise<ISuccessGeneric | IErrorGeneric> {
    try {
      await this.subscribeToMeetup(request);
      return { statusCode: 200, success: true, message: "Inscripcion exitosa" } as ISuccessGeneric;
    } catch (error) {
      return { statusCode: 500, success: false, message: 'Ocurrio un error, contacte con el Administrador del sistema' } as IErrorGeneric;
    }
  }

  @Response(200, "Order send correctly", "Ya le enviamos el mail a tu proveedor")
  @Response<IErrorGeneric>(500, "Order send failed", {
    success: false,
    message: "Ocurrio un error, contacte con el Administrador del sistema",
  })
  @Get('/realizar-pedido/{token}')
  public async makeOrderBirras(@Path() token: string) {
    try {
      const service = new BirrasMeetupService();
      return service.makeOrder(token);
    } catch (error) {
      return { statusCode: 500, success: false, message: 'Ocurrio un error, contacte con el Administrador del sistema' } as IErrorGeneric;
    }
  }

  @Response(200, "Order send correctly", "Ya le enviamos el mail a tu proveedor")
  @Response<IErrorGeneric>(500, "Order send failed", {
    success: false,
    message: "Ocurrio un error, contacte con el Administrador del sistema",
  })
  @Security('api_key')
  @Post('/my-meetups')
  public async myMeetups(@Request() request: Req) {
    try {
      return MeetupRepository.getMeetupsUser(request)
    } catch (error) {
      return { statusCode: 500, success: false, message: 'Ocurrio un error, contacte con el Administrador del sistema' } as IErrorGeneric;
    }
  }

}
