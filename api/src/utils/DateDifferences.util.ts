import moment from 'moment';

export function HoursDifference(dateToCompare: Date | string) {
  const startTime = moment();
  const end = moment(dateToCompare);

  const duration = moment.duration(end.diff(startTime));
  return parseInt(String(duration.asHours()));

}

export function ApproachDate(startDate, objectWithDate, dateKey) {
  const eventDay = moment(startDate)
  const next = objectWithDate
    .map((s) => moment(s[dateKey], "YYYY/MM/DD HH:mm:ii"))
    .sort((m) => m.valueOf())
    .find((m) => m.isAfter(eventDay));
  return next.format("YYYY-MM-DD HH:mm:ss")
}
