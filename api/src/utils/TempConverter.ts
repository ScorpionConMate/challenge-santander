export function KelvinToCelsius(temp: number) {
  return Number((temp - 273.15).toFixed(1));
}
