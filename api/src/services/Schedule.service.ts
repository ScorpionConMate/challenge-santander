import { scheduleJob } from "node-schedule";
import { BirrasMeetupService } from './BirrasMeetup.service';
import { MeetupService } from './Meetup.service';

export function ScheduleService() {
  return scheduleJob('* * * * *', () => {
    console.log("Actualizo cantidad de birras por meetup")
    MeetupService.updateQuantityBirras();

    console.log("Seteo el clima para la meetup");
    // new BirrasMeetupService().setClimaMeetup()

  });
}
