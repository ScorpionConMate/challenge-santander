import * as nodemailer from "nodemailer";
import Mail from 'nodemailer/lib/mailer';
import { SMPT_PASS, SMTP_SERVICE, SMTP_USER } from '../config/constants';

interface SendMailOptions {
  from?: string;
  to: string;
  subject: string;
  html: string;
}
export async function SendMail(options: SendMailOptions) {
  const transporter = nodemailer.createTransport({
    service: SMTP_SERVICE,
    port: 587,
    auth: {
      pass: SMPT_PASS,
      user: SMTP_USER,
    },
  });

  const mailOptions: Mail.Options = {
    from: options.from || '"Meetups Globales" <meetup.info@birras.com>',
    to: options.to,
    subject: options.subject,
    html: options.html
  }
  return transporter.sendMail(mailOptions, function (err, info) {
    if (err) {
      console.error(err);
    }
  });
}
