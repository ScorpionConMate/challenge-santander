import "reflect-metadata";
import { createConnection, getConnection } from "typeorm";
import express from "express";
import * as bodyParser from "body-parser";
import cors from "cors";
import { join } from 'path';
import routes from './routes/routes';
import morgan from "morgan";
import swaggerUi from "swagger-ui-express";
import { ScheduleService } from './services/Schedule.service';

createConnection().then(async connection => {
  // Increase max listener to prevent memory limit.
  process.setMaxListeners(50);

  // Try establish database connection

  // Init express app
  const app = express();
  app.use(express.json());
  app.use(morgan("tiny"));

  const dirPath = join(__dirname, "storage");

  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
  app.use(cors());

  app.use(express.static(dirPath));
  app.use(express.static("./swagger"));
  app.use('/api-docs',
    swaggerUi.serve, swaggerUi.setup(
      {},
      {
        swaggerUrl: "/api",
        swaggerOptions: {
          url: "/swagger.json",
        },
      }
    )
  );

  // Register all application routes
  app.use("/api/", routes);

  // Run Schedule
  ScheduleService();

  app.listen(process.env.APP_PORT, () => {
    console.log(`⚡️[server]: Server is running at ${process.env.APP_URL}`);
  });
}).catch(err => console.log(err));


