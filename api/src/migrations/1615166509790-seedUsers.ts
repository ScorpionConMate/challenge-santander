import { MigrationInterface, QueryRunner } from "typeorm";
import * as faker from "faker";
import { User } from '../models/User';

enum Roles {
  ADMIN = 'admin',
  USER = 'user'
}

export class seedUsers1615166509790 implements MigrationInterface {
  name = 'seedUsers1615166509790'

  public async up(queryRunner: QueryRunner): Promise<void> {
    try {
      for (let index = 0; index < 20; index++) {
        const user = new User();
        user.role = index <= 5 ? Roles.ADMIN : Roles.USER;
        user.password = index <= 5 ? 'admin' : 'usuario';
        user.nombre = faker.name.firstName();
        user.apellido = faker.name.lastName();
        user.email = (index <= 5 ? 'admin' : 'usuario') + index + "@birras.com";
        // Hashing password because Subscriber no detect inserts in migrations
        user.hashPassword();
        await user.save()
      }
    } catch (error) {
      console.log(error)
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query("ALTER TABLE `user` CHANGE `deletedAt` `deletedAt` datetime(6) NULL DEFAULT 'NULL'");
  }

}
