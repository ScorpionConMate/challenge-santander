import { MigrationInterface, QueryRunner } from "typeorm";
import { Meetup } from '../models/Meetup';
import * as faker from "faker";
import moment from 'moment';
import { MeetupImages } from '../models/MeetupImages';
import { User } from '../models/User';
import * as provincia from "../utils/provincias.json";
export class seedMeetupTables1615169050404 implements MigrationInterface {
  name = 'seedMeetupTables1615169050404'

  public async up(queryRunner: QueryRunner): Promise<void> {
    try {
      for (let index = 0; index < 50; index++) {
        const meetup = new Meetup();
        const usuId = Number(((Math.random() * 5) + 1).toFixed(0));
        const dur = String(parseInt(String(Math.random() * (5 - 1) + 1))) + "h";
        const days = parseInt(String(Math.random() * 3 + 3));
        const province = parseInt(String(Math.random() * 23 + 1));
        const fecha = moment().add("days", days).format('YYYY-MM-DD HH:mm:ss');

        meetup.organizador = await User.findOne(usuId);
        meetup.nombre = faker.name.jobTitle();
        meetup.fecha = moment(fecha).toDate()
        meetup.duracion = dur;
        meetup.lugar = `${provincia[province]}, AR`;
        meetup.descripcion = faker.lorem.paragraph();

        meetup.slugify();

        const meetImage = new MeetupImages()
        meetImage.imagePath = `https://dummyimage.com/1920x420/${faker.internet.color().replace('#', '')}/${faker.internet.color().replace('#', '')}`
        meetImage.save()
        meetup.image = meetImage;
        await meetup.save()
      }
    } catch (error) {
      console.log(error);
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
  }

}
