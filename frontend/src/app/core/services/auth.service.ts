import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import jwtDecode from 'jwt-decode';
import * as moment from 'moment';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private baseUrl = 'auth';
  constructor(private apiService: ApiService, private router: Router) {}

  public login(email, password) {
    const payload = {
      email,
      password,
    };
    return this.apiService
      .post(`${this.baseUrl}/login`, payload)
      .subscribe((arg) => this.setSession(arg));
  }

  private setSession(authResult) {
    localStorage.setItem('token', authResult.token);
    this.router.navigate(['/']);
  }

  private getSession() {
    if (localStorage.getItem('token')) {
      return {
        token: localStorage.getItem('token'),
      };
    }
  }

  isLoggedIn() {
    if (this.getSession()) {
      const expire: any = jwtDecode(this.getSession().token);
      return moment().isAfter(expire.exp);
    }
    return false;
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['']);
    });
  }

  infoSession() {
    if (this.getSession()) {
      const decoded: any = jwtDecode(this.getSession().token);
      return decoded.usuario;
    }
  }

  isAdmin() {
    if (this.getSession()) {
      const user = this.infoSession();
      if (user.role === 'admin') {
        return true;
      }
      return false;
    }
  }
}
