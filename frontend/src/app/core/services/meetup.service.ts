import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class MeetupService {

  constructor(public apiService: ApiService) {
  }

  getMeetups() {
    return this.apiService.get("meetup").toPromise();
  }

  getMeetup(slug) {
    return this.apiService.get(`meetup/${slug}`).toPromise();
  }

  createMeetup(data) {
    return this.apiService.post("meetup/new", data).toPromise();
  }

  subscribeToMeetup(id) {
    return this.apiService.post(`meetup/subscribe/${id}`, {}).toPromise();
  }
}
