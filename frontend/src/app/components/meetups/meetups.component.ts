import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { MeetupService } from "../../core/services/meetup.service";

@Component({
  selector: 'app-meetups',
  templateUrl: './meetups.component.html',
  styleUrls: ['./meetups.component.scss'],
})
export class MeetupsComponent implements OnInit {

  meetups: any;
  isLoading = true
  constructor(
    private meetupService: MeetupService,
    private router: Router
    ) {
  }

  ngOnInit(): void {
    this.getMeetups()

  }

  getMeetups(){
    this.meetupService.getMeetups()
    .then(res => {
      this.meetups = res;
      this.isLoading = false;
    }).catch(err => {
      console.error("Error trying meetups")
    });
  }

  goToMeetup(slug) {
    return this.router.navigate(['meetup', slug]);
  }
}
